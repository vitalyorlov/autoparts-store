package com.d1l.controller;

import com.opensymphony.xwork2.ActionSupport;
import com.d1l.service.DocumentGenerator;
import org.apache.struts2.interceptor.ServletResponseAware;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class DocumentController extends ActionSupport implements ServletResponseAware {

    private HttpServletResponse response;
    private int id;

    private void makeResponse(ByteArrayOutputStream stream, String contentType, String fileName) throws IOException {
        response.setContentType(contentType);
        response.setHeader("Content-Disposition",
                "inline; filename=" + fileName);
        response.setContentLength(stream.size());

        OutputStream os = response.getOutputStream();
        os.write(stream.toByteArray());
        os.flush();
        os.close();
        stream.reset();
    }
    @Override
    public String execute() throws Exception {

        return super.execute();
    }

    public String getOrderPDF() throws IOException {
        makeResponse(DocumentGenerator.generateOrderInPDFById(getId()), "application/pdf", "order.pdf");
        return NONE;
    }

    public String getOrdersXLS() throws IOException {
        makeResponse(DocumentGenerator.generateOrdersInXLS(), "application/vnd.ms-excel", "orders.xls");
        return NONE;
    }
    public String getOrdersCSV() throws IOException {
        makeResponse(DocumentGenerator.generateOrdersInCSV(), "text/csv", "orders.csv");
        return NONE;
    }

    public String getWarehousePDF() throws IOException {
        makeResponse(DocumentGenerator.generateWarehousesInPDFById(getId()), "application/pdf", "warehouse.pdf");
        return NONE;
    }

    public String getWarehousesXLS() throws IOException {
        makeResponse(DocumentGenerator.generateWarehousesInXLS(), "application/vnd.ms-excel", "warehouses.xls");
        return NONE;
    }
    public String getWarehousesCSV() throws IOException {
        makeResponse(DocumentGenerator.generateWarehousesInCSV(), "text/csv", "warehouses.csv");
        return NONE;
    }

    public String getCarPDF() throws IOException {
        makeResponse(DocumentGenerator.generateCarsInPDFById(getId()), "application/pdf", "car.pdf");
        return NONE;
    }

    public String getCarsXLS() throws IOException {
        makeResponse(DocumentGenerator.generateCarsInXLS(), "application/vnd.ms-excel", "cars.xls");
        return NONE;
    }
    public String getCarsCSV() throws IOException {
        makeResponse(DocumentGenerator.generateCarsInCSV(), "text/csv", "cars.csv");
        return NONE;
    }
    public void setServletResponse(HttpServletResponse httpServletResponse) {
        this.response = httpServletResponse;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
